#define _CRT_SECURE_NO_WARNINGS
#include"head.h"
//初始化栈




void StackInit(Stack* ps)
{
	assert(ps);

	ps->a = NULL;
	ps->top = 0;
	ps->capacity = 0;


}

// 入栈 
void StackPush(Stack* ps, STDataType data)
{
	assert(ps);
	//够空间的话就不开空间直接赋值
	//不够的话，也就是top此时=capaciy了，这个时候realoc一个新的空间；
	
	if (ps->top == ps->capacity)
	{
		int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		STDataType* tmp = (STDataType*)realloc(ps->a, newcapacity * sizeof(STDataType));
		if(tmp == NULL)
		{
			printf("realloc failed!");
			exit(-1);
		}

		ps->a = tmp;
		ps->capacity = newcapacity;
	}


	ps->a[ps->top] = data;
	ps->top++;

}

// 出栈 
void StackPop(Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));

	ps->top--;
}
// 获取栈顶元素 

STDataType StackTop(Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));
	return ps->a[ps->top-1];
}

int StackSize(Stack* ps)
{
	assert(ps);

	return ps->top;
}
// 检测栈是否为空，如果为空返回非零结果，如果不为空返回0 

int StackEmpty(Stack* ps)
{
	if (ps->top)
		return 0;
	else
		return 1;
}
// 销毁栈 
void StackDestroy(Stack* ps)
{
	free(ps->a);
	ps->capacity = ps->top = 0;

}


