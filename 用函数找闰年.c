
#include <stdio.h>

int check(int a)


{
	if (((a % 4 == 0) && (a % 100 != 0)) || (a % 400 == 0))
		return 1;
	else
		return 0;
}


int main()
{
	int year = 0;
	for (year = 1000; year <= 2000; year++)
	{
		if (check(year))
		{
			printf("%d ", year);
		}
	}
}