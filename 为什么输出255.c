#include<stdio.h>
int main()
{
  char a[1000] = {0};
  int i=0;
  for(i=0; i<1000; i++)
  {
    a[i] = -1-i;
  }
  printf("%d",strlen(a));
  return 0;
}
//这玩意为啥输出255？
//首先不难看出来，这玩意的for循环从0开始不断自增直到999，对于数组里的数来讲则是从-1到-1000
//而strlen则是计算遇到'\0'之前的字符量，而\0本身的数值就是0，这段需要打印的本质则是这个数组0之前有几个数字。
//但这个数组本身是个char类型的数组，对于char类型的数组来讲，它本身所能存储的数据大小是-128 - 127 
//为什么？char类型本身只能存放1个字节，即8bit位0000000
//这样的话，对于char类型的数据，不管怎么增大。都只会因为截断的存在而无法打破存放大小的限制。
//那么，根据如上的这个圈圈，这个数组从-1开始不断-1，到达0之前有128+127个数字，strlen就会计数到255停止，所以本题输出255.