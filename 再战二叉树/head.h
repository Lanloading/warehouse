#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef  int   BTDataType;


typedef struct BTNode
{
	BTDataType data;
	struct BTNode* left;
	struct BTNode* right;
}BT;


// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树

BT* BinaryTreeCreate(BTDataType* a, int n, int* pi);

// 二叉树销毁

void BinaryTreeDestory(BT** root);

// 二叉树节点个数

int TreeSize(BT* root)
{
	if (root == NULL)
		return 0;

	return TreeSize(root->left) + TreeSize(root->right) + 1;
}

// 二叉树叶子节点个数
int BinaryTreeLeafSize(BT* root)
{
	if (root == NULL)
		return 0;
	if (root->left == NULL && root->right == NULL)
		return 1;

	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}

//二叉树高度
int BinaryTreeLeafHeight(BT* root)
{
	if (root == NULL)
		return 0;

	int left = BinaryTreeLeafHeight(root->left) + 1;
	int right = BinaryTreeLeafHeight(root->right) + 1;

	if (left > right)
		return left;
	else
		return right;

}


// 二叉树第k层节点个数

int BinaryTreeLevelKSize(BT* root, int k)
{
	assert(k > 0);
	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;

	return BinaryTreeLevelKSize(root->left, k - 1) + BinaryTreeLevelKSize(root->right, k - 1);


}

// 二叉树查找值为x的节点

BT* BinaryTreeFind(BT* root, BTDataType x)
{
	if (root == NULL)
		return NULL;

	if (root->data == x)
		return root;

	BT* leftret = BinaryTreeFind(root->left, x);

	if (leftret)
		return leftret;

	BT* rightret  = BinaryTreeFind(root->right, x);

	if (rightret)
		return rightret;
	return NULL;

}


//前序遍历 根 左子树 右子树
void preOrder(BT* root)
{

	if (root == NULL)
	{
		printf("NULL ");
		return;
	}


	printf("%d ", root->data);
	preOrder(root->left);
	preOrder(root->right);
}



//中序遍历
// 
void MidOrder(BT* root)
{

	if (root == NULL)
	{
		printf("NULL ");
		return;
	}


	preOrder(root->left);
	printf("%d ", root->data);

	preOrder(root->right);
}



// 
// 
//后序遍历
void leftOrder(BT* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}


	preOrder(root->left);
	preOrder(root->right);
	printf("%d ", root->data);

}



bool CompleteTree(BT* root)
{
	QNode q;
	QueueInit(&q);

	if (root)
		QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		BT* guard = QueueFront(&q);
		QueuePop(&q);
		//持续遍历，直到碰到空节点。
		if (guard == NULL)
			break;

		QueuePush(&q, guard->left);
		QueuePush(&q, guard->right);

	}

	while (!QueueEmpty(&q))
	{
		BT* guard = QueueFront(&q);
		QueuePop(&q);
		//此时已经碰到了空节点，如果碰到了非空节点，直接返回false
		if (guard != NULL)
		{
			QueueDestroy(&q);
			return false;
		}

	}


	QueueDestroy(&q);

	return true;
}
