#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int compare(void* e1, void* e2)
{
	return(*(int*)e1 - *(int*)e2);
}


int main()
{
	int arr[9] = { 8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int i = 0;


	qsort(arr, sz, sizeof(arr[0]), compare);

	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}

	return 0;
}  //mark up
  