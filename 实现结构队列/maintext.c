#define _CRT_SECURE_NO_WARNINGS
#include"head.h"

void Text()
{
	Queue q;

	QueueInit(&q);
	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);
	QueueDestroy(&q);
}

int main()
{
	Text();
	return 0;
}