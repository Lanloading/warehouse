#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "head.h"

void Print(int* a, int n)
{
	for (int i = 0; i < n; ++i)
		printf("%d ", a[i]);
}

void Swap(int* x, int* y)
{
	int tmp = *x;
	*x = *y;
	*y = tmp;

}



void InsertSort(int* a, int n)
{
	for (int i = 0; i < n - 1; ++i)
	{
		int end = i;
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (a[end] > tmp)
			{
				a[end + 1] = a[end];
				end--;
			}
			else
				break;
		}
		a[end + 1] = tmp;

	}
}
//三数取中
int GetMidIndex(int* a, int left, int right)
{
	int mid = left + (right - left) / 2;

	if (a[mid] > a[left])
	{
		if (a[mid] < a[right])
			return mid;
		else if (a[right] < a[left])
			return left;
		else
			return right;
	}
	else
	{
		if (a[mid] > a[right])
			return mid;
		else if (a[left] < a[right])
			return left;
		else
			return right;
	}
}

void QuickSort(int* a, int left, int right)
{
	if (left >= right)
		return;

	//int keyi = QSortPart1(a, left, right);

	//QuickSort(a, left, keyi - 1);
	//QuickSort(a, keyi + 1, right);

	if (right - left <= 8)
	{
		InsertSort(a + left, right - left + 1);
	}
	else
	{
		int keyi = QSortPart2(a, left, right);

		QuickSort(a, left, keyi - 1);
		QuickSort(a, keyi + 1, right);
	}
}


//霍尔法
int  QSortPart1(int* a, int left,int right )
{

	int mid = GetMidIndex(a, left, right);
	Swap(&a[mid], &a[left]);

	int keyi = left;

	while (left < right)
	{
		while (right >left && a[right] >=a[keyi])
			right--;
		while (right > left && a[left] <= a[keyi])
			left++;

		if(left<right)
		Swap(&a[left], &a[right]);

	}
	int newkey = left;

	Swap(&a[left], &a[keyi]);

	return newkey;

}

//挖坑法
int  QSortPart2(int* a, int left, int right)
{
	//三数取中
	int mid = GetMidIndex(a, left, right);
	Swap(&a[mid], &a[left]);

	int hole = left;
	int key = a[left];
	while (left < right)
	{
		//右边向左走，找到左边的坑
		while (right > left && a[right] >= key)
			right--;

		//找到了，填洞换洞
		Swap(&a[right], &a[hole]);
		hole = right;

		//左边向右走，找到右边的坑
		while (right > left && a[left] <= key)
			left++;


		//找到了，填洞换洞
		Swap(&a[left], &a[hole]);
		hole = left;
	}

	a[left] = key;
	return hole;
}
//前后指针
int  QSortPart3(int* a, int left, int right)
{
	int mid = GetMidIndex(a, left, right);
	Swap(&a[mid], &a[left]);

	int keyi = left;
	int key = a[left];

	int cur = left +1 , prev = left;
	while (cur <= right)
	{
		// 找小
		if (a[cur] < a[keyi] && ++prev != cur)
			Swap(&a[cur], &a[prev]);

		++cur;
	}

	Swap(&a[keyi], &a[prev]);
	return prev;
}
//非递归快速排序
void Qsortother(int* a, int begin, int end)
{
	Stack st;
	StackInit(&st);

	//先往栈里扔入整个区间范围，让循环启动
	StackPush(&st, begin);
	StackPush(&st, end);

	while (!StackEmpty(&st))
	{
		int right = StackTop(&st);
		StackPop(&st);

		int left = StackTop(&st);
		StackPop(&st);

		//求取当前区间Key值
		int key = QSortPart3(a, left, right);

		//得到Key值后，借由key划分两个区间,分别入栈


		//区间分配切割完毕的标志是left >= key -1 时，此时不必再次入栈，再等一轮pop使得栈为空结束循环
		if (left < key - 1)
		{
			StackPush(&st, left);
			StackPush(&st, key - 1);
		}

		if (key + 1 < right)
		{
			StackPush(&st, key + 1);
			StackPush(&st, right);
		}
	}

	//排序结束，销毁栈
	StackDestroy(&st);


}



void BubbleSort(int* a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n - i; ++j)
		{
			if (a[j] < a[j - 1])
				Swap(&a[j], &a[j - 1]);
		}
	}
}

void SelectSort(int* a, int n)
//疯狂遍历整个数组，每一次遍历找最小值和最大值，最大值放末尾，最小值放前头
{
	int begin = 0, end = n - 1;
	while (begin < end)
	{
		int mini = begin, maxi = end;
		for (int i = begin+1; i <= end; ++i)
		{
			if (a[i] < a[mini])
			{
				mini = i;
			}

			if (a[i] > a[maxi])
			{
				maxi = i;
			}

		}
		Swap(&a[mini], &a[begin]);
		if (maxi == begin)
			maxi = mini;
		Swap(&a[maxi], &a[end]);
	

		++begin;
		--end;


	}

}



void SeerSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 2;
		for (int j = 0; j < gap; ++j)
		{
			for (int i = j; i < n - gap; i += gap)
			{
				int end = i;
				int tmp = a[end + gap];


				while (end >= 0)
				{
					if (a[end] > tmp)
					{
						a[end + gap] = a[end];
						end -= gap;
					}
					else
						break;
				}
				a[end + gap] = tmp;

			}
		}
	}
	
}


void _MergeSort(int* a, int begin, int end, int* tmp)
{
	//什么时候结束递归？同快排类似
	if (begin >= end)
		return;

	//求一下分割点，也就是中间值
	int mid = (end +begin) / 2;

	//先分割，分割完事儿了再归并
	_MergeSort(a, begin, mid, tmp);
	_MergeSort(a, mid + 1, end, tmp);


	//归并 取小的尾插
	//按照升序将数据存放入tmp中

	int begin1 = begin, end1 = mid;
	int begin2 = mid+1, end2 = end;

	int count = begin;

	while (begin1 <= end1 && begin2 <= end2 )
	{
		if (a[begin1] <= a[begin2])
			tmp[count++] = a[begin1++];
		else
			tmp[count++] = a[begin2++];

	}
	//寻找小的环节已经结束，这个时候剩下的那个区间内所有剩下的数据一股脑传进去就好

	while(begin1<=end1)
		tmp[count++] = a[begin1++];

	while(begin2<=end2)
		tmp[count++] = a[begin2++];
	

	// 拷贝回原数组 -- 归并哪部分就拷贝哪部分回去
	memcpy(a + begin, tmp + begin, (end - begin + 1) * sizeof(int));

}



void MergeSort(int* a, int n)
{

	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc failed!");
		return;
	}
	_MergeSort(a, 0, n - 1, tmp);

	free(tmp);
	tmp = NULL;
}