#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
#include<string.h>
//
//创建一个整形数组，完成对数组的操作
//
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//
//要求：自己设计以上函数的参数，返回值。
void  reverse(int arr1[],int sz)

{
	int left = 0;
	int right  = sz-1;
	int tmp = 0;

	while (left < right)
	{
		tmp = arr1[left];
		arr1[left] = arr1[right];
		arr1[right] = tmp;

		left++;
		right--;
	}
}
void first(int arr[], int sz)
{
	int i = 0;

	for (i = 0; i < sz; i++)

	{

		arr[i] = 0;

	}
}
void Print(int arr[], int sz)

{

	int i = 0;

	for (i = 0; i < sz; i++)

	{

		printf("%d ", arr[i]);

	}

	printf("\n");

}
int main()
{
	int arr1[] = { 1,2,3,4,5,6 };
	int i = 0;

	int sz = sizeof(arr1) / sizeof(arr1[0]);

	Print(arr1, sz);

	reverse(arr1, sz);

	Print(arr1, sz);

	first(arr1, sz);

	Print(arr1,sz);

}




