void exchange(int arr[], int sz)
{
	int left = 0;
	int right = sz - 1;
	int change = 0;
	while (left < right)
	{
		while (arr[left] % 2 == 1)
		{
			left++;
		}
		while (arr[right] % 2 == 0)
		{
			right--;
		}
		if (left < right)
		{
			change = arr[left];
			arr[left] = arr[right];
			arr[right] = change;
		}
	}
}

int main()
{
	int sz = 0;
	int arr[] = { 1,2,3,4,5,6,7,8,9 };
	int i = 0;

	sz = sizeof(arr) / sizeof(arr[0]);
	exchange(arr, sz);

	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;

}