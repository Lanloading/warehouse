#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
#include<string.h>
#include <math.h>
#include <assert.h>


char* my_strcpy(char* des,const char* res)//利用const保证res和不会因为位置对调而报错
										  //原理是const修饰的变量不能被改变，当赋值顺序改变的时候，触发const机制
										  //即*res ++ = des ++ 的之后报错，扼杀可能报错的来源
{
	assert(res != NULL);//断言，放置啥都不输入就执行函数
	assert(des != NULL);

	char* ret = des;
	while (*des++ = *res++ )
	{
		;
	}
	return ret;//为什么返回一个指针变量呢？方便链式访问，数组名相当于一个指针变量，返回char*就相当于返回了整个数组
}

int main()
{
	char des[] = "XXXXXXXXXXX";
	char res[] = "copy down";

	//my_strcpy(des, res);

	printf("%s\n", my_strcpy(des, res));


	return 0;
}
//mark here

//mark next