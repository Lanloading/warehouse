#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
int output(int a)
{
    int request = a;
    int i = 0;
    int result;
    for (i = 1; i <= request; i++)
    {
        result = i* request;
        printf("%d * %d = %d\n", i, request,result);
    }
    return  0;
}

int main()
{
    int a = 0;
    scanf("%d", &a);
    output (a);
    return 0;
}