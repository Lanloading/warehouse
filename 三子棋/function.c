#define _CRT_SECURE_NO_WARNINGS
#include"head.h"


//初始化棋盘
void intboard(char board[ROW][COL],int  row, int col)
{
	int i = 0;
	int n = 0;
	for (i = 0; i < row; i++)
	{
		for (n = 0; n < col; n++)
		{
			board[i][n] =' ';
		}
	}
}



//绘制棋盘
void drawboard(char board[ROW][COL], int  row, int col)
{
	int i = 0;
	
	for (i = 0; i < row; i++)
	{
		int n = 0;
		for (n = 0; n < col; n++)
		{
			printf(" %c ", board[i][n]);//每过3个字符打印一次|，空格之间放入棋盘数组中的元素
			if (n < col - 1)
			{
				printf("|");
			}
		}
		printf("\n");
		if (i < row - 1)
		{
			int n = 0;
			for (n = 0; n < col; n++)
			{
				printf("---");
				if (n < col - 1)
					printf("|");
			}
			printf("\n");
		}
	}
}



void game(char board[ROW][COL], int row, int col)
{
	char ret = 'n';


	do
	{
		playerturn(board, ROW, COL);
		ret = check(board, ROW, COL);

		if (ret == '*')
			break;

		comturn(board, ROW, COL);
		ret = check(board, ROW, COL);

		if (ret == '#')
			break;


	} while (ret == 'c');
	if (ret == '*')
		printf("玩家获胜!\n");
	else if (ret == '#')
		printf("电脑获胜！\n");
	else 
		printf("平局！\n");

}


//玩家下棋
void playerturn(char board [ROW][COL],int row , int col)
{
	int a = 0;
	int b = 0;

	while (1)
	{
		printf("玩家的回合！\n请输入坐标下棋.>");

		scanf("%d %d", &a, &b);

		if (board[a - 1][b - 1] == ' ')
		{
			board[a - 1][b - 1] = '*';
			drawboard(board, ROW, COL);
			break;

		}
		else
		{

			printf("坐标输入错误或已有棋子 请重新输入\n");

			printf("\n");

			drawboard(board, ROW, COL);
		}
	}

}



//电脑下棋
void comturn(char board[ROW][COL], int row, int col)
{
	int x = 0;
	int y = 0;

	printf("电脑的回合！\n");


	while (1)
	{
		y = rand() % col;//限制取数的大小，防止电脑下到其他位置去。
		x = rand() % row;

		if (board[x][y] ==' ')
		{
			board[x][y] = '#';
			drawboard(board, ROW, COL);
			printf("\n");
			break;

		}
	}
}

char check(char board[ROW][COL], int row, int col)
{
	int a = 0;
	int b = 0;
	//行检测
	for (a = 0; a < row; a++)
	{
		if (board[a][0] == board[a][1] && board[a][1] == board[a][2] && board[a][1] != ' ')
		return board[a][1];	
	}
	//列检测
	for (b = 0; b < col; b++)
	{
		if (board[0][b] == board[1][b] && board[1][b] == board[2][b]  && board[1][b] != ' ')
		return board[1][b];
		
	}
	//对角线检测00
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ')
	{
		return board[0][0];
	}
	else if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
	{
		return  board[0][0];
	}
	if(doubleko(board[ROW][COL], row, col))
	{
		return 'd';
	}

	return 'c';
}


//平局判断，只要场上没有空格且没有赢家的时候处罚
int doubleko(char board[ROW][COL], int row, int col)
{
	int a = 0;
	int b = 0;

	for (a = 0; a < row; a++)
	{
		for (b = 0; b < col; b++)
		{
			if (board[a][b] == ' ');
			return 0;
		}
	}
	return 1;
}

