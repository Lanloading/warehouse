#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <time.h>
#define ROW 3
#define COL 3
void intboard(char board[ROW][COL], int  row, int col);

void drawboard(char board[ROW][COL], int  row, int col);

void game(char board[ROW][COL], int row, int col);

void playerturn(char board[ROW][COL], int row, int col);

void comturn(char board[ROW][COL], int row, int col);

char check(char board[ROW][COL], int row, int col);

int doubleko(char board[ROW][COL], int row, int col);

/////mark up