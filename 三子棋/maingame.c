#define _CRT_SECURE_NO_WARNINGS
#include"head.h"
//1.要先有棋盘，初始化一个棋盘，棋盘有行有列，为了方便之后扩大自定义棋盘大小使用define定义变量方便修改
//2.要拿一个数组存放棋子数据，初始化棋盘时，塞入空格
//3.接下来就是下棋，获得玩家下棋所输入的坐标，-1后加入board数组中
//4.电脑下棋的逻辑也写完了，在目前不太聪明的版本中，利用随机数进行下棋。
//5.接下来是回合制的实现，需要加入游戏结束的判断，分别为玩家胜利，电脑胜利，平局
//6.
void menu()
{
	printf("======= 三子棋 =======\n");
	printf("======= 1.play =======\n");
	printf("======= 0.exit =======\n");
	printf("======================\n");
}

int main()
{
	menu();
	srand((unsigned int)time(NULL));

	char board[ROW][COL] = { 0 };
	intboard(board, ROW, COL);
	int input = 0;
	int palyer = 0;

	while (1)
	{
		scanf("%d", &input);
		if (input == 1)
		{
			printf("游戏开始！\n");

			drawboard(board, ROW, COL);

			game(board,ROW,COL);

			menu();

		}
		else if (input == 0)
		{
			printf("退出游戏\n");
			break;
		}
		else
		{
			printf("输入错误，请重新选择！\n");
		}

	}
	return 0;
}

