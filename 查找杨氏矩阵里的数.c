#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//作业内容
//
//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的
// ，请编写程序在这样的矩阵中查找某个数字是否存在。
//
//1  3  5  7  9
//3  5  7  9  11
//5  7  9  11 13 
//
//
//
//

int check(int n)
{
	return n % 2;
}


int main()
{
	int arr[20][20] = { 0 };
	int i = 0;
	int j = 0;
	int a = 1;
	int b = 0;
	int n = 0;
	for (i = 0; i < 20; i++)
	{
		arr[i][0] = a;
		b = 0;
		for (j = a; j < 20+a; j+=2)
		{
			arr[i][b] = j;
			b++;
		}
		a += 2;

	}

	scanf("%d", &n);
	if (check(n))
	{
		printf("这个数在矩阵中存在\n");
	}
	else
		printf("这个数在矩阵中不存在\n");
	return 0;
}    