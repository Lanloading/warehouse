#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
//实现一个函数，判断一个数是不是素数。
//
//利用上面实现的函数打印100到200之间的素数。
int output(int a)
{
    int i = 0;
    for (i = 2; i < a; i++)
    {
        if (a % i == 0)
        return 0;
    }
    return  1;
}

int main()
{
    int a = 0;
    for (a = 101; a <= 200; a += 2)
    {
        if (output(a))
        printf("%d\n", a);

    }
    return 0;
}



