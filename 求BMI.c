#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
int main()
{
        int a, b;
        double c;
        a = 0, b = 0;
        c = 0.0;
        scanf("%d %d", &a, &b);
        c = a / ((b / 100.0) * (b / 100.0));
        //使用浮点类型时，一定要加小数点！！！
        printf("%.2f", c);
        return 0;
    
}