int myPOW(int n,int k)
{
    if (k != 1)
    {
        return n * myPOW(n, k - 1);
    }
    else
        return n;
}

int main()
{
    int n = 0;
    int k = 0;
    int ret = 0;

    scanf("%d %d", &n,&k);

    ret = myPOW(n,k);

    printf("%d ",ret);
}
