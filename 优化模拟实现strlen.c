#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h >

int mystrlen(const char* arr)
{
	int count = 0;
	while (*arr++)
	{
		count++;
	}
	return count;
}


int main()
{
	char arr[50] = "XXXXX";

	scanf("%s", arr);

	printf("%d ", mystrlen(arr));
	
	return 0;

}