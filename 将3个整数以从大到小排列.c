#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

//写代码将三个整数数按从大到小输出。


//例如：

//

//输入：2 3 1

//

//输出：3 2 1

int main()

{

   int a,b,c,change;

   change = 0;

   scanf("%d %d %d", &a, &b, &c);

   if (a < b)

   {

       change = a;

       a = b;

       b = change;

   }

   if (c > a)

   {

       change = a;

       a = c;

       c = change;

   }

    if (b < c)

   {

       change = b;

       b = c;

       c = change;

   }



   printf("%d %d %d", a, b, c);

   return 0;



}