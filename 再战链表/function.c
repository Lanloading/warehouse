#define _CRT_SECURE_NO_WARNINGS

#include "head.h"


// 动态申请一个结点
SListNode* BuySListNode(SLTDateType x)
{

	SListNode* newnode = (SListNode* )malloc(sizeof(SListNode));
	if (newnode == NULL)
	{
		perror("malloc fail!");
		exit(-1);
	}
	newnode->val = x;

	newnode->next = NULL;

	return newnode;
}




// 单链表打印
void SListPrint(SListNode* phead)
{

	SListNode* cur = phead;

	while (cur != NULL)
	{
		printf("%d->", cur->val);
		cur = cur->next;
	}
	printf("NULL\n");
}


// 单链表的头插
void SListPushFront(SListNode** pphead, SLTDateType x)
{
	assert(pphead);

	SListNode* newnode = BuySListNode(x);
	newnode->next = *pphead;
	*pphead = newnode;

}

// 单链表头删
void SListPopFront(SListNode** pphead)
{
	assert(pphead);

	SListNode* cur = *pphead;

	*pphead = cur->next;

	free(cur);

	cur = NULL;

}

// 单链表尾插
void SListPushBack(SListNode** pphead, SLTDateType x)
{

	assert(pphead);

	SListNode* newnode = BuySListNode(x);

	if ((*pphead)->next == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		SListNode* cur = *pphead;
		while (cur->next != NULL)
		{
			cur = cur->next;
		}
		cur->next = newnode;
	}
}

// 单链表的尾删
void SListPopBack(SListNode** pphead)
{

	assert(pphead);


	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SListNode* cur = *pphead;
		SListNode* prev = NULL;

		while (cur->next != NULL)
		{
			prev = cur;
			cur = cur->next;
		}
		free(cur);
		prev->next = NULL;
		cur = NULL;
	}


}

// 单链表查找
SListNode* SListFind(SListNode* pphead, SLTDateType x)
{


	SListNode* cur = pphead;

	while (cur->next !=NULL )
	{
		if (cur->val == x)
		{
			return cur;
		}
		cur = cur->next;
	}

	return NULL;

}


// 在pos后面插入
void SListInsertAfter(SListNode* pos, SLTDateType x)
{
	SListNode* newnode = BuySListNode(x);

	newnode->next = pos->next;
	pos->next = newnode;

}


// 在pos之前插入
void SListInsert(SListNode** pphead, SListNode* pos, SLTDateType x)
{
	assert(pphead);
	SListNode* newnode = BuySListNode(x);

	SListNode* cur = *pphead;

	while (cur->next != pos)
	{
		cur = cur->next;
	}

	newnode->next = pos;
	cur->next = newnode;

}


// 删除pos位置
void SListErase(SListNode** pphead, SListNode* pos)
{
	assert(pphead);
	assert(pos);

	if (*pphead == pos)
	{
		SListPopFront(pphead);
	}

	SListNode* cur = *pphead;

	while (cur->next != pos)
	{
		cur = cur->next;

		assert(cur);
	}

	cur->next = pos->next;

	free(pos);
	pos = NULL;
}

// 删除pos后面位置
void SListEraseAfter(SListNode* pos)
{
	assert(pos);
	if (pos->next == NULL)
	{
		return;
	}
	else
	{
		SListNode* cur = pos->next;

		pos->next = pos->next->next;

		free(cur);
	}

}