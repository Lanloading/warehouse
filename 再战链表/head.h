#define _CRT_SECURE_NO_WARNINGS


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
// 1、无头+单向+非循环链表增删查改实现
typedef int SLTDateType;

typedef struct SListNode
{
	SLTDateType val;
	struct SListNode* next;

}SListNode;


// 动态申请一个结点
SListNode* BuySListNode(SLTDateType x);

	
// 单链表打印
void SListPrint(SListNode* phead);

// 单链表尾插
void SListPushBack(SListNode** pphead, SLTDateType x);

// 单链表的头插
void SListPushFront(SListNode** pphead, SLTDateType x);

// 单链表的尾删
void SListPopBack(SListNode** pphead);

// 单链表头删
void SListPopFront(SListNode** pphead);

// 单链表查找
SListNode* SListFind(SListNode* pphead, SLTDateType x);


// 在pos之前插入
void SListInsert(SListNode** pphead, SListNode* pos, SLTDateType x);

// 在pos后面插入
void SListInsertAfter(SListNode* pos, SLTDateType x);

// 删除pos位置
void SListErase(SListNode** pphead, SListNode* pos); 

// 删除pos后面位置
void SListEraseAfter(SListNode* pos);

// 分析思考为什么不在pos位置之前插入？
因为不用再次遍历数组