#define _CRT_SECURE_NO_WARNINGS
#include "head.h"
void text()
{
	SListNode* SLT = NULL;

	SListPushFront(&SLT, 1);
	SListPushFront(&SLT, 2);
	SListPushFront(&SLT, 3);
	SListPushFront(&SLT, 4);

	SListPrint(SLT);

	SListPopFront(&SLT);

	SListPrint(SLT);
	SListPushBack(&SLT, 1);
	SListPushBack(&SLT, 2);

	SListPrint(SLT);

	SListPopBack(&SLT);
	SListPrint(SLT);

	SListNode* ret = SListFind(SLT, 2);
	ret->val *= 10;
	SListPrint(SLT);


}


void text2()
{

	SListNode* SLT = NULL;


	SListPushFront(&SLT, 1);
	SListPushFront(&SLT, 2);
	SListPushFront(&SLT, 3);
	SListPushFront(&SLT, 4);
	SListPrint(SLT);


	// 在pos后面插入
	SListNode* ret = SListFind(SLT, 2);
	SListInsertAfter(ret, 3);
	SListPrint(SLT);

	// 在pos之前插入
	SListNode* ret2 = SListFind(SLT, 3);
	SListInsert(&SLT, ret2, 3);
	SListPrint(SLT);

	// 删除pos位置
	printf("删除pos位置\n");
	SListNode* ret3 = SListFind(SLT, 3);
	SListErase(&SLT, ret3);
	SListPrint(SLT);


	// 删除pos后面位置
	printf("删除pos后面的位置\n");

	SListNode* ret4 = SListFind(SLT, 3);
	SListEraseAfter(ret4);
	SListPrint(SLT);



}

int main()
{
	text2();
	return 0;
}