#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>
void reverse(char* left, char* right)
{
    while (left < right)
    {
        char tmp = *left;
        *left = *right;
        *right = tmp;

        left++;
        right--;
    }
}

int main()
{
    int i = 0;
    int sz = 0;
    char arr1[101] = {0};

    gets(arr1);

    sz = strlen(arr1);
    
    reverse(arr1, arr1+sz-1);

    char *start = arr1;

    while (*start)
    {
        char*end = start;
        while (*end != ' ' && *end != '\0')
        {
            end++;
        }
        reverse(start, end-1);
        if (*end != '\0')
        {
            end++;
        }
        start = end;
    }

    printf("%s\n", arr1);
    return 0;
}
//mark up
