#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
#include<string.h>
#include <stdio.h>
int check(int a)
{
    if (((a % 4 == 0) && (a % 100 != 0)) || (a % 400 == 0))
        return 1;
    else
        return 0;
}


int main()
{
    int year = 0;
    int month = 0;
    int arr1[12] = { 31,29,31,30,31,30,31,31,30,31,30,31 };
    int arr2[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };


    while (scanf("%d %d", &year, &month)!=EOF)
    {

        if (check(year))
        {
            printf("%d", arr1[month - 1]);
        }
        else
        {
            printf("%d", arr2[month - 1]);

        }
        printf("\n");
    }
    return 0;
}