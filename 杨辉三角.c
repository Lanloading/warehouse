#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//作业内容
//
//在屏幕上打印杨辉三角。
//
//
//1                 n = 1
//					
//1 1				n = 2
//
//1 2 1				n = 3
//
//1 3 3 1			n = 4
// 
//1 4 6 4 1			n = 5
//
//1 5 10 10 5 1		n = 6
//
//1 6 15 20 15 6 1	n = 7

void function(int n)
{
	int data[30][30] = { 1 }; //第一行直接填好，播下种子

	int i, j;

	for (i = 1; i < n; i++) //从第二行开始填

	{
		data[i][0] = 1; //每行的第一列都没有区别，直接给1，保证不会越界。
		for (j = 1; j <= i; j++) //从第二列开始填
		{
			data[i][j] = data[i - 1][j] + data[i - 1][j - 1]; //递推方程
		}
	}
	for (i = 0; i < n; i++) //填完打印

	{
		for (j = 0; j <= i; j++)

		{

			printf("%d ", data[i][j]);

		}
		putchar('\n');
	}
}


int main()
{
	int a = 0;

	scanf("%d", &a);

	function(a);

	return 0;

}