#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
#include<string.h>
#include <math.h>
//
//
//作业内容
//
//求出0～100000之间的所有“水仙花数”并输出。
//
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。



int main()
{
	int a = 0;
	int i = 0;

	for (a = 0; a < 100000; a++)
	{
		int count = 1;
		int c = a;
		int g = 0;

		while (c / 10)
		{
			c = c / 10;
			count++;
		}
		c = a;
		while (c)
		{
			g += pow((c%10), count);
			c = c / 10;
		}
		if (a == g)
		{
			printf("%d ", a);
		}

	}

	return 0;
}