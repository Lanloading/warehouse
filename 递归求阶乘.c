#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
int  Fun(int n)
{
    if (n > 1)
    {
        return n * Fun(n - 1);
    }
    else
        return 1;

}
int main()
{
    int a = 0;

    scanf("%d", &a);

    int ret = Fun(a); 

    printf("%d ",ret);

    return 0;
}



