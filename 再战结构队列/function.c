#define _CRT_SECURE_NO_WARNINGS
#include "head.h"


// 初始化队列 

void QueueInit(Queue* q)
{
	assert(q);
	q->head = q->end = NULL;
	q->size = 0;
}

// 队尾入队列 
void QueuePush(Queue* q, QDataType data)
{
	assert(q);


	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("malloc fail!");
		exit(-1);
	}
	else
	{
		newnode ->data = data;
		newnode ->next = NULL;
	}

	if (q->end == NULL)
	{
		q->end = q->head = newnode;

	}
	else
	{
		q->end->next = newnode;
		q->end = newnode;

	}

	q->size++;

}

// 队头出队列 

void QueuePop(Queue* q)
{
	assert(q);
	assert(!QueueEmpty(q));

	if (q->head->next == NULL)
	{
		free(q->head);
		q->head = q->end = NULL;
	}
	else
	{
		QNode* prev = q->head;
		q->head = q->head->next;
		free(prev);
		prev = NULL;
	}
	q->size--;

}

// 获取队列头部元素 

QDataType QueueFront(Queue* q)
{
	assert(q);
	assert(!QueueEmpty(q));

	return q->head->data;

}

// 获取队列队尾元素 

QDataType QueueBack(Queue* q)
{
	assert(q);
	assert(!QueueEmpty(q));

	return q->end->data;
}


// 获取队列中有效元素个数 

int QueueSize(Queue* q)
{
	assert(q);

	return q->size;
}

// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
bool QueueEmpty(Queue* q)
{

	assert(q);

	if (q->end == NULL || q->head == NULL)
	{
		return true;
	}
	else
		return false;
}


// 销毁队列 

void QueueDestroy(Queue* q)
{
	assert(q);
	QNode* cur = q->head;
	while (cur)
	{
		QNode* del = cur;
		cur = cur->next;
		free(del);
	}


	q->head = q->end = NULL;
}