#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
#include<string.h>
//编程实现：两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？
//
//输入例子 :
//
//1999 2299
//
//输出例子 : 7
int main()
{
    int a = 0;
    int b = 0;
    int count = 0;
    int i = 0;
    scanf("%d %d", &a, &b);
    
    for (i = 0; i < 32; i++)
    {
        if(((a >> i) & 1) != ((b >> i) & 1))
        {
            count++;
        }
    }
    printf("%d", count);
    return 0;
}