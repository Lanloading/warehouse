#include <stdio.h>
int main()
{
	int n = 0;
	int ret = 0;
	int i = 1;
	scanf("%d", &n);
	while (n)
	{
		if ((n % 10) % 2)//在此处鉴定奇数偶数，一旦鉴定是奇数，把当前的i加给ret，i每一次循环都在乘以10，一旦
                        //没有成功触发赋值，相当于跳过这一位，ret从i是1的时候开始增加，每一次都会成功的对到正确的位置，非常巧妙的代码
			ret += i;
		i *= 10;
		n /= 10;
	}
	printf("%d", ret);
}