#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//写一个代码打印1 - 100之间所有3的倍数的数字
int main()
{
    int a, i;
    for (i = 1; i < 101; i++)
    {
        if (0 == i % 3)
            printf("%d\n", i);
    }

    return 0;


}
