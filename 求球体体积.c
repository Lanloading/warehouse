#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
int main()
{
    float a;
    double b, c;
    a = 0.0;
    c = 3.1415926;
    scanf("%f", &a);
    b = (4.0 / 3) * c * pow(a, 3);
    //int 类型里，4/3得到的是一个1而不是小数点，会导致计算错误
    printf("%.3f", b);
    return 0;

}