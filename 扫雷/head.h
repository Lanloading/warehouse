#define _CRT_SECURE_NO_WARNINGS
#include  <stdio.h>
#include <stdlib.h>
#include <time.h >
#define ROW 9
#define COL 9
#define ROWS 11
#define COLS 11
#define easy_level 10

void DrawMineField(char mine[ROWS][COLS], int row, int col, char n);

void IntMineField(char mine[ROWS][COLS], int row, int col);

void ReadyForMine(char  mine[ROWS][COLS], int row, int col);

void game(char  mine[ROWS][COLS], char  showmine[ROWS][COLS], int row, int col, int rows, int cols);

int  countmine(char  mine[ROWS][COLS], int x, int y);

int Check(char  mine[ROWS][COLS], int x, int y);