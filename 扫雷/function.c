#define _CRT_SECURE_NO_WARNINGS
#include "head.h";
//绘制雷区，地雷情况，同时可以实现对雷区的初始化，只需要将n在绘制前赋值进每个元素内即可
int minenumber = easy_level;


void DrawMineField(char mine[ROWS][COLS], int row, int col, char n)
{
	int i = 0;
	int j = 0;

	printf("________OwO________\n");



	for (j = 0; j < col+1; j++)
	{
		printf("%d ", j);
	} 

	printf("\n");

	for (i = 1; i <=row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			if (n == '0' || n == '*')//如果输入字符0或者*，则初始化，不输入就纯打印
			{
				mine[i][j] = n;//之后若是需要让其发挥初始化功能从这下手
			}
			printf("%c ", mine[i][j]);
		}

		printf("\n");

	}
	printf("===================\n");

}
//随机埋雷
void ReadyForMine(char  mine[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int i = 0;
	int count = 0;
	
	while (count < 10)
	{
		x = rand() % row+1;
		y = rand() % col+1;

		if (mine[x][y] != '1')
		{
			mine[x][y] = '1';
			count++;
		}
	}

}

//初始化雷区
void IntMineField(char mine[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;

	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			mine[i][j] = '0';
		}
	}
}




//游戏主体逻辑
void game(char  mine[ROWS][COLS], char  showmine[ROWS][COLS], int row, int col, int rows, int cols)
{
	int x = 0;
	int y = 0;
	int minenumber = 0;
	int ret = 1;
	int ret2 = 1;
	int c = 0;

	IntMineField(mine, ROWS, COLS);//初始化雷区
	DrawMineField(showmine, ROW, COL, '*');//初始化雷区后打印外面的隐藏雷区

	ReadyForMine(mine, ROW, COL);//埋雷
	//DrawMineField(mine, ROW, COL, 'k');//此处解除注释即可在初始化的时候看见雷的部署情况

	do
	{
		printf("请选择接下来的动作：\n1.探查地雷 2.为确认的地雷插下旗帜\n");
		scanf("%d", &c);
		if (2 == c)
		{
			printf("请输入需要 插下旗帜 的坐标->.");
			scanf("%d %d", &x, &y);
			ret2 = Flag(mine, showmine, x, y);
			if (ret2 == 2)//当Flag函数返回2的时候跳出循环，进行胜利的判断
				break;

		}
		else if (1 == c)
		{
			printf("请输入需要 排查 的坐标->.");
			scanf("%d %d", &x, &y);

			ret = Check(mine, x, y);//判断玩家是否踩到了雷

			if (showmine[x][y] != '*')
			{
				printf("该坐标被排查过了，不能重复排查\n");
			}

			else if (ret == 1)
			{
				Spread(mine, showmine, x, y);//展开函数
			}
			DrawMineField(showmine, ROW, COL, 'k');//打印排查完毕后的雷区状况
		}
		else
		{
			printf("输入错误，请重新输入！\n");

		}
	

	} while (ret);

	if (ret == 0)
	{
		printf("寄！你被炸死了,下辈子小心！XD\n");
		DrawMineField(mine, ROW, COL, 'k');//玩家失败，展示真实雷区情况
	}
	if (ret2 == 2)
	{
		printf("NICE！你排完了所有的地雷，高手！");
	}

	
	//DrawMineField(showmine, ROW, COL, 'k');
}


//获得坐标处旁的地雷数量
int  countmine(char  board[ROWS][COLS], int x,int y)
{
	int count = board[x - 1][y - 1] + board[x - 1][y] + board[x - 1][y + 1] +
				board[ x ]  [y - 1] + board[x][y + 1] +
				board[x + 1][y - 1] + board[x + 1][y] + board[x + 1][y + 1] - 8 * '0';
	return count;
}



int Check(char  mine[ROWS][COLS], int x, int y)
{
	if (mine[x][y] == '1')
	{
		return 0;
	}
	else
		return 1;
}


//展开的逻辑：
//该坐标不是雷 ^
//该坐标周围没有雷 ^
//该坐标的附近一旦有雷，则不要进入展开函数，也就是countmine函数返回的值是0 ^
//该坐标没有被排查过^
//先查询真实雷区内的雷的部署情况，在递归的时候为蒙盖雷区赋值
void Spread(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y)
{
	int offset_x = 0;
	int offset_y = 0;
	int count = 0;
	//坐标合法
	if (x >= 1 && x <= 9 && y >= 1 && y <= 9)
	{
		//遍历周围坐标
		for (offset_x = -1; offset_x <= 1; offset_x++)
		{
			for (offset_y = -1; offset_y <= 1; offset_y++)
			{
				//如果这个坐标不是雷
				if (mine[x + offset_x][y + offset_y] == '0')
				{
					//统计周围雷的个数
					count = countmine(mine, x + offset_x, y + offset_y);
					if (count == 0)
					{
						if (show[x + offset_x][y + offset_y] == '*')
						{
							show[x + offset_x][y + offset_y] = ' ';
							Spread(mine, show, x + offset_x, y + offset_y);
						}
					}
					else
					{
						show[x + offset_x][y + offset_y] = count + '0';
					}
				}
			}
		}
	}
}


//插旗子：

int  Flag(char  mine[ROWS][COLS], char  showmine[ROWS][COLS], int x, int y)
{
	if (showmine[x][y] == '*')
	{
		showmine[x][y] = '@';
		DrawMineField(showmine, ROW, COL, 'k');

		if (mine[x][y] == '1')
		{
			minenumber--;
			printf("%d\n", minenumber);
			if (minenumber == 0)
			{
				return 2;
			}
			return 1;
		}
		return 1;
	}
	else
	{
		printf("坐标非法，请重新输入！");
	}
}
