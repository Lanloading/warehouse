#define _CRT_SECURE_NO_WARNINGS
#include "head.h";
//绘制雷区，地雷情况，同时可以实现对雷区的初始化，只需要将n在绘制前赋值进每个元素内即可
void DrawMineField(char mine[ROWS][COLS], int row, int col, char n)
{
	int i = 0;
	int j = 0;

	printf("________OwO________\n");



	for (j = 0; j < col+1; j++)
	{
		printf("%d ", j);
	} 

	printf("\n");

	for (i = 1; i <=row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			if (n == '0' || n == '*')//如果输入字符0或者*，则初始化，不输入就纯打印
			{
				mine[i][j] = n;//之后若是需要让其发挥初始化功能从这下手
			}
			printf("%c ", mine[i][j]);
		}

		printf("\n");

	}
	printf("===================\n");

}
//随机埋雷
void ReadyForMine(char  mine[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int i = 0;
	int count = 0;
	
	while (count < 10)
	{
		x = rand() % row+1;
		y = rand() % col+1;

		if (mine[x][y] != '1')
		{
			mine[x][y] = '1';
			count++;
		}
	}

}

//初始化雷区
void IntMineField(char mine[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;

	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			mine[i][j] = '0';
		}
	}
}




//游戏主体逻辑
void game(char  mine[ROWS][COLS], char  showmine[ROWS][COLS], int row, int col, int rows, int cols)
{
	int x = 0;
	int y = 0;
	int minenumber = 0;
	int ret = 1;
	int wincount = 0;

	IntMineField(mine, ROWS, COLS);
	DrawMineField(showmine, ROW, COL, '*');//初始化雷区后打印外面的隐藏雷区

	ReadyForMine(mine, ROW, COL);//埋雷
	DrawMineField(mine, ROW, COL, 'k');

	do
	{
		printf("请输入需要排查的坐标->.");
		scanf("%d %d", &x, &y);

		ret = Check(mine, x, y);

		if (showmine[x][y] != '*')
		{
			printf("该坐标被排查过了，不能重复排查\n");
		}


		else if (ret == 1)
		{
			minenumber = countmine(mine, x, y);//查找附近的雷子
			if (minenumber == 0)
			{
				SpreadOut(mine,showmine, x, y);
			}
			showmine[x][y] = minenumber + '0';



			wincount++;
		}
		 if (wincount == row * col - easy_level)
		{
			printf("NICE！你排完了所有的地雷，高手！");
			break;
		}


		DrawMineField(showmine, ROW, COL, 'k');

	} while (ret);

	if (ret == 0)
	{
		printf("寄！你被炸死了,下辈子小心！XD\n");
		DrawMineField(mine, ROW, COL, 'k');
	}

	
	//DrawMineField(showmine, ROW, COL, 'k');
}


//获得坐标处旁的地雷数量
int  countmine(char  board[ROWS][COLS], int x,int y)
{
	int count = board[x - 1][y - 1] + board[x - 1][y] + board[x - 1][y + 1] +
				board[ x ]  [y - 1] + board[x][y + 1] +
				board[x + 1][y - 1] + board[x + 1][y] + board[x + 1][y + 1] - 8 * '0';
	return count;
}



int Check(char  mine[ROWS][COLS], int x, int y)
{
	if (mine[x][y] == '1')
	{
		return 0;
	}
	else
		return 1;
}


//展开的逻辑：
//该坐标不是雷 ^
//该坐标周围没有雷 ^
//该坐标的附近一旦有雷，则不要进入展开函数，也就是countmine函数返回的值是0 ^
//该坐标没有被排查过^
// 
// 
//先查询真实雷区内的雷的部署情况，在递归的时候为蒙盖雷区赋值
void SpreadOut(char truemine[ROWS][COLS], char showmine[ROWS][COLS], int x, int y)
{
	int minenumber = 0;

	if (x > 0 && y > 0 && x < 9 && y < 9)
	{
		//printf("It has been spread\n");
		//showmine[x][y] != '*' &&
		if (countmine(truemine, x, y) == 0)
		{
			PutZero(showmine, x, y);
			SpreadOut(truemine, showmine, x + 1, y);
			SpreadOut(truemine, showmine, x, y + 1);

		}

		else
		{
			minenumber = countmine(truemine, x, y);
			showmine[x][y] = minenumber + '0';

		}


	}

}

//为确认可扩散的坐标附近8格进行赋值0
void PutZero(char  board[ROWS][COLS], int x, int y)
{
		board[x - 1][y - 1] = '0';
		board[x - 1][y + 1] = '0';
		board[x + 1][y - 1] = '0';
		board[x + 1][y + 1] = '0';
		board[x][y - 1] = '0';
		board[x][y + 1] = '0';
		board[x + 1][y] = '0';
		board[x - 1][y] = '0';
}