#define _CRT_SECURE_NO_WARNINGS

#include "head.h"

//顺序表的初始化
void SeqInit(SL* slt)
{
	assert(slt);

	slt->a = NULL;

	slt->size = slt->capacity = 0;

}

//销毁
void SLDestory(SL* slt)
{
	assert(slt);


	free(slt->a);
	slt->a = NULL;
	slt->capacity =slt->size = 0;

}



void SLPrint(const SL* slt)
{
	assert(slt);
	for (int i = 0; i < slt->size; i++)
	{
		printf("%d ", slt->a[i]);
	}
	printf("\n");

}

void CheckSeq(SL* slt)
{
	//扩容
	if (slt->size == slt->capacity)
	{
		int newcapacity = slt->capacity == 0 ? 4 : slt->capacity * 2;
		Datatype* tmp = (Datatype*)realloc(slt->a, newcapacity * sizeof(Datatype));
		if (realloc == NULL)
		{
			perror("realloc fail!");
			exit(-1);
		}

		slt->a = tmp;
		slt->capacity = newcapacity;
	}

}



//尾插
void SeqListPushBack(SL* slt, Datatype x)
{
	assert(slt);
	
	CheckSeq(slt);

	slt->a[slt->size] = x;

	slt->size++;

}




void SeqListPopBack(SL* slt)
{
	assert(slt);
	assert(slt->size > 0);  //顺序表不能为空
	slt->size--;
}


//头插
void SeqListPushFront(SL* slt, Datatype x)
{
	assert(slt);
	assert(slt->size > 0);


	CheckSeq(slt);

	for (int i = slt->size; i >= 0 ; i--)
	{
		slt->a[i + 1] = slt->a[i];
	}

	slt->a[0] = x;
	slt->size++;

}


//头删
void SeqListPopFront(SL* slt)
{
	assert(slt);

	assert(slt->size > 0);

	for (int i = 0; i < slt -> size - 1; i++)
	{

		slt->a[i] = slt->a[i + 1];
	}
	slt->size--;

}


//顺序表查找
int SeqListFind(SL* slt, Datatype x)
{
	assert(slt);

	assert(slt->size > 0);

	for (int i = 0; i < slt->size; i++)
	{
		if (slt->a[i] == x)
		{
			return i + 1;
		}

	}
	printf("can't found!\n");
	return -1;
}



//插入
void SeqListInsert(SL* slt, size_t pos, Datatype x)
{
	assert(slt);

	assert(pos <= slt->size);


	CheckSeq(slt);

	int end = slt->size;

	while (end > pos)
	{
		slt->a[end] = slt->a[end-1];
		end--;
	}


	slt->a[pos] = x;
	slt->size++;


}



//定向位置删除
void SeqListErase(SL* slt, size_t pos)
{

	assert(slt);

	int cur = pos;
	
	while (cur < slt->size)
	{
		slt->a[cur] = slt->a[cur + 1];
		cur++;
	}

	slt->size--;

}


void SLModify(SL*slt, size_t pos, Datatype  x)
{
	assert(slt);
	assert(pos < slt->size);

	slt->a[pos] = x;
}

