#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int  Datatype;

typedef struct Seqlist
{
	Datatype* a;
	int size;
	int capacity;

}SL;


void SeqInit(SL* slt);

void SLPrint(const SL* slt);

void CheckSeq(SL* slt);
//
void SLDestory(SL* slt);
//
void SeqListPushBack(SL* slt, Datatype x);
// ˳���β��
//
void SeqListPopBack(SL* slt);
//// ˳���βɾ
//
void SeqListPushFront(SL* slt, Datatype x);
//// ˳���ͷ��
//
void SeqListPopFront(SL* slt);
//// ˳���ͷɾ
//
int SeqListFind(SL* slt, Datatype x);
////˳�������
//
void SeqListInsert(SL* slt, size_t pos, Datatype x);
//// ˳�����posλ�ò���x
//
void SeqListErase(SL*slt, size_t pos);
//// ˳���ɾ��posλ�õ�ֵ
//
void SLModify(SL* slt, size_t pos, Datatype x);

