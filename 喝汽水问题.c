int main()
{
    int money = 0;
    int soda = 0;
    int bottle = 0;
    
    scanf("%d", &money);

    soda = money;
    bottle = money;

    while (bottle > 1)
    {
        soda += bottle / 2;
        bottle = bottle / 2 + bottle % 2;
    }

    printf("%d", soda);

    return 0;
}//mark up
