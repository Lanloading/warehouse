#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
#include<string.h>
//实现一个对整形数组的冒泡排序
 // 8 7 6 5 4 3 2 1
 // 0 1 2 3 4 5 6 7
void bubble(int arr[],int sz)
{
	int i = 0;
	int tmp = 0;
	int n = 0;

	for (i = 0; i < sz - 1 ; i++)
	{
		for (n = 0; n < sz - i -1; n++)
		{
			if (arr[n] > arr[n + 1])
			{
				tmp = arr[n];
				arr[n] = arr[n + 1];
				arr[n + 1] = tmp;
			}
		
		}
	}
}

int main()
{
	int arr[] = { 8,7,6,5,4,3,2,1 };
	int n = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);


	bubble(arr,sz);
	for(n = 0; n < sz; n++)
	{
		printf("%d ", arr[n]);
	}
	return 0;
}




