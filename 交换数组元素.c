#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
#include<string.h>

//将数组A中的内容和数组B中的内容进行交换。（数组一样大）
void  exchange(int arr1[],int arr2[],int sz)

{
	int i = 0;
	int tmp = 0;
	for (i = 0; i < sz; i++)
	{
		tmp = arr1[i];
		arr1[i] = arr2[i];
		arr2[i] = tmp;
	}
}

int main()
{
	int arr1[] = { 1,2,3,4,5,6 };
	int arr2[] = { 6,5,4,3,2,1 };
	int i = 0;

	int sz = sizeof(arr1) / sizeof(arr1[0]);

	exchange(arr1, arr2,sz);

	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr1[i]);
	}
	printf("\n");

	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr2[i]);

	}


}




