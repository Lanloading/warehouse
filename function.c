#define _CRT_SECURE_NO_WARNINGS
#include"head.h"


BT* CreatTree()
{
	BT* n1 = (BT*)malloc(sizeof(BT));
	assert(n1);
	BT* n2 = (BT*)malloc(sizeof(BT));
	assert(n2);
	BT* n3 = (BT*)malloc(sizeof(BT));
	assert(n3);
	BT* n4 = (BT*)malloc(sizeof(BT));
	assert(n4);
	BT* n5 = (BT*)malloc(sizeof(BT));
	assert(n5);
	BT* n6 = (BT*)malloc(sizeof(BT));
	assert(n6);

	n1->left = n2;
	n1->right = n4;
	n2->left = n3;
	n2->right = NULL;
	n3->left = NULL;
	n3->right = NULL;
	n4->left = n5;
	n4->right = n6;
	n5->left = NULL;
	n5->right = NULL;
	n6->left = NULL;
	n6->right = NULL;


	n1->data = 1;
	n2->data = 2;
	n3->data = 3;
	n4->data = 4;
	n5->data = 5;
	n6->data = 6;

	return n1;
}




int main()
{
	BT* root;

	root = CreatTree();

	//preOrder(root);
	//printf("\n");

	//MidOrder(root);
	//printf("\n");


	//leftOrder(root);
	//printf("\n");

	printf("二叉树的节点个数为：%d ", TreeSize(root));
	printf("\n");

	printf("二叉树的叶子节点个数为：%d ", BinaryTreeLeafSize(root));
	printf("\n");


	
	printf("二叉树的第K层的节点个数为：%d ", BinaryTreeLevelKSize(root,1));
	printf("\n");

	printf("二叉树的最大深度为：%d ", BinaryTreeLeafHeight(root, 1));
	printf("\n");

	



	return 0;
}
