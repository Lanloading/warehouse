#define _CRT_SECURE_NO_WARNINGS
#include "head.h"
void Text()
{
	Stack st;

	StackInit(&st);

	StackPush(&st, 1);
	StackPush(&st, 2);
	StackPush(&st, 3);
	StackPush(&st, 4);

	StackPrint(&st);

	StackPop(&st);
	StackPop(&st);
	StackPrint(&st);

	StackPush(&st, 1);
	StackPush(&st, 2);
	StackPrint(&st);
	printf ("The Top of the Stack is:%d \n",StackTop(&st));

	printf("The  Size of the Stack is:%d \n", StackSize(&st));

}

int main()
{

	Text();

	return 0;
}