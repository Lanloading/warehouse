#define _CRT_SECURE_NO_WARNINGS
#include "head.h"


//初始化栈
void StackInit(Stack* ps)
{
	ps->a = NULL;

	ps->capacity = 0;
	ps->top = 0;

}

//打印栈
void StackPrint(Stack* ps)
{
	assert(ps);

	for (int i = 0; i < ps->top; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");

}

// 入栈 
void StackPush(Stack* ps, STDataType data)
{
	assert(ps);

	if (ps->top == ps->capacity)
	{
		int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		STDataType* tmp = (STDataType*)realloc(ps->a, newcapacity * sizeof(STDataType));
		if (tmp == NULL)
		{
			perror("realloc fail!");
			exit(-1);
		}
		ps->a = tmp;
		ps->capacity = newcapacity;
	}

	ps->a[ps->top] = data;
	ps->top++;

}

// 出栈 
void StackPop(Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));

	ps->top--;

}

// 获取栈顶元素 
STDataType StackTop(Stack* ps)
{
	assert(ps);

	if (ps->top == 0)
	{
		printf(" no data in this Stack!\n");
		return;
	}

	return ps->a[ps->top - 1];


}

// 获取栈中有效元素个数 
int StackSize(Stack* ps)
{
	assert(ps);


	if (ps->top == 0)
	{
		return 0;
	}

	return ps->top;


}

// 检测栈是否为空，如果为空返回非零结果，如果不为空返回0 
int StackEmpty(Stack* ps)
{

	assert(ps);

	if (ps->top)
		return 0;
	else
		return 1;


}

// 销毁栈 
void StackDestroy(Stack* ps)
{
	assert(ps);

	free(ps->a);

	ps->a = NULL;

	ps->capacity = 0;
	ps->top = 0;


}