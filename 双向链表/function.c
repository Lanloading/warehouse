#define _CRT_SECURE_NO_WARNINGS
#include "head.h"

ListNode* creatspace(LTDataType x)
{

	ListNode* newspace = (ListNode*)malloc(sizeof(ListNode));

	if (newspace == NULL)
	{
		printf("malloc failed!");
		exit(-1);
	}

	newspace->data = x;
	newspace->prev = NULL;
	newspace->next = NULL;

	return newspace;
}


// 创建返回链表的头结点.
ListNode* ListCreate()
{
	ListNode* Listhead = (ListNode*)malloc(sizeof(ListNode));

	if (Listhead == NULL)
	{
		printf("malloc failed!");
		exit(-1);
	}

	Listhead->next = Listhead;
	Listhead->prev = Listhead;


	return Listhead;
}

// 双向链表打印

void ListPrint(ListNode* pHead)
{
	assert(pHead);
	ListNode* cur = pHead->next;

	

	printf("head=>");

	while (cur != pHead)
	{
		printf("%d<=>", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

 //双向链表销毁

void ListDestory(ListNode* pHead)
{
	assert(pHead);

	ListNode* cur = pHead ->next;
	ListNode* next = NULL;

	while (cur!= pHead)
	{
		next = cur->next;
		free(cur);
		cur = next;
	}
	free(pHead);
}


// 双向链表尾插

void ListPushBack(ListNode* pHead, LTDataType x)
{
	assert(pHead);

	ListNode* newnode = creatspace(x);

	ListNode* cur = pHead->prev;
	//while (cur->next != pHead)
	//{
	//	cur = cur->next;
	//}

	cur->next = newnode;
	pHead->prev = newnode;
	newnode->next = pHead;
	newnode->prev = cur;
}

// 双向链表尾删

void ListPopBack(ListNode* pHead)
{
	assert(pHead);

	ListNode* cur = pHead->prev;
	ListNode* prev =pHead->prev->prev;

	pHead->prev = prev;

	free(cur);

	prev->next = pHead;
	
	cur = NULL;
}

// 双向链表头插

void ListPushFront(ListNode* pHead, LTDataType x)
{
	assert(pHead);


	// 先链接newnode 和 phead->next节点之间的关系
	ListNode* newnode = creatspace (x);
	newnode->next = pHead->next;
	pHead->next->prev = newnode;

	pHead->next = newnode;
	newnode->prev = pHead;

// 不关心顺序
//LTNode* newnode = BuyListNode(x);
//LTNode* first = phead->next;
//phead->next = newnode;
//newnode->prev = phead;
//newnode->next = first;
//first->prev = newnode;


}

// 双向链表头删

void ListPopFront(ListNode* pHead)
{
	assert(pHead);

	ListNode* cur = pHead->next;

	pHead->next = cur->next;

	cur->next->prev = pHead;
}
// 双向链表查找

ListNode* ListFind(ListNode* pHead, LTDataType x)
{
	assert(pHead);

	ListNode* cur = pHead->next;
	if (x == 0)
	{
		return cur;
	}
	while (cur != pHead)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;

}

// 双向链表在pos的前面进行插入

void ListInsert(ListNode* pos, LTDataType x)
{

	assert(pos);

	ListNode* newnode = creatspace(x);

	newnode->next = pos;
	newnode->prev = pos->prev;

	pos->prev->next = newnode;
	pos->prev = newnode;


}

// 双向链表删除pos位置的节点

void ListErase(ListNode* pos)
{
	assert(pos);

	pos->prev->next = pos->next;
	pos->next->prev = pos->prev;
	free(pos);
}