#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<stdlib.h>
#include <time.h>
//19%10==9
//19/10==1
//199/10==19
//递归方式实现打印一个整数的每一位
//2333
void Fun(int n)
{
    if (n > 9)
    {
         Fun(n / 10);
    }
    printf("%d ", n % 10);

}
int main()
{
    int a = 0;
    scanf("%d", &a);
    Fun(a);
    return 0;
}



