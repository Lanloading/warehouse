#define _CRT_SECURE_NO_WARNINGS
#include "SeqList.h"

void Text2()
{
	Seqlist S;
	IntSeq(&S);
	CheckSeq(&S);

	SeqListPushBack(&S, 1);
	SeqListPushBack(&S, 2);
	SeqListPushBack(&S, 3);
	SeqListPushBack(&S, 4);


	SLPrint(&S);
	SeqListPopBack(&S);
	SLPrint(&S);

	SeqListPushFront(&S, 55);
	SLPrint(&S);

	SeqListInsert(&S, 0, 600);
	SLPrint(&S);

	SeqListErase(&S, 3);
	SLPrint(&S);


	SLDestory(&S);


}


int main()
{
	Text2();

	return 0;	
}