#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
typedef int Datatype; 

typedef struct Seqlist
{
	Datatype* arr;	//拿来扩充的数组
	size_t size;    //存放几个数据	
	size_t capcity; //存放数据的大小
}Seqlist;

void IntSeq(Seqlist* pseq);

void SLPrint(const Seqlist* pseq);

void CheckSeq(Seqlist* pseq);

void SLDestory(Seqlist* pseq);

void SeqListPushBack(Seqlist* psl, Datatype x);
// 顺序表尾插

void SeqListPopBack(Seqlist* psl);
// 顺序表尾删

void SeqListPushFront(Seqlist* psl, Datatype x);
// 顺序表头插

void SeqListPopFront(Seqlist* psl);
// 顺序表头删

int SeqListFind(Seqlist* psl, Datatype x);
//顺序表查找

void SeqListInsert(Seqlist* psl, size_t pos, Seqlist*  x);
// 顺序表在pos位置插入x

void SeqListErase(Seqlist* psl, size_t pos);
// 顺序表删除pos位置的值

void SLModify(Seqlist* psl, size_t pos, Datatype x);

//mark up
