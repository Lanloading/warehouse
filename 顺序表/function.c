#define _CRT_SECURE_NO_WARNINGS
#include "SeqList.h"
// 基本增删查改接口
// 顺序表初始化
void CheckSeq(Seqlist* pseq)//检查顺序表内是否需要扩容,需要的话向后扩容两倍
{
	assert(pseq);

	if (pseq->size == pseq->capcity)
	{
		int newcapcity = pseq->capcity == 0 ? 4 : pseq->capcity * 2;
        //当size == capacity的时候容量就爆了，记得扩容，用realloc扩容。
		Datatype* newarr = (Datatype*)realloc(pseq->arr, newcapcity * sizeof(Datatype));
		if (newarr == NULL)
		{
			perror("内存空间开辟失败");
			exit(-1);
		}
		pseq->arr = newarr;
		pseq->capcity = newcapcity;


	}


}
void IntSeq(Seqlist* pseq)//初始化顺序表里的内容
{
	assert(pseq);

	pseq->arr = NULL;
	pseq->capcity = pseq->size = 0;

}
void SLPrint(const Seqlist* pseq)//打印顺序表里的内容
{
	assert(pseq);

	int i = 0;
	for (i = 0; i < pseq->size; ++i)
	{
		printf("%d ", pseq->arr[i]);
	}
	printf("\n");
}
void SLDestory(Seqlist* pseq)//清除所有结构体内的内容
{

	free(pseq->arr);
	pseq->arr = NULL;
	pseq->capcity = pseq->size = 0;
	
}
void SeqListPushBack(Seqlist* pseq, Datatype x)
// 顺序表尾插
{
	assert(pseq);
	CheckSeq(pseq);

	pseq->arr[pseq->size] = x;
	pseq->size++;
}

void SeqListPopBack(Seqlist* pseq)
// 顺序表尾删
{
	assert(pseq);
	assert(pseq->size > 0);//暴力检查，千万不能让size越界访问了
	pseq->size--;

}

void SeqListPushFront(Seqlist* pseq, Datatype x)
//顺序表头插
{

	assert(pseq);
	assert(pseq->size > 0);
	CheckSeq(pseq);
	int end = pseq->size - 1;
	while (end >= 0)
	{
		pseq->arr[end+1] = pseq->arr[end];
		--end;
	}
	pseq->arr[0] = x;
	pseq->size++;
}
void SeqListPopFront(Seqlist* pseq)
// 顺序表头删
{
	assert(pseq);
	assert(pseq->size > 0);

	CheckSeq(pseq);
	int end = 0;
	while (end < pseq->size)
	{
		pseq->arr[end] = pseq->arr[end+1];
		++end;
	}

	pseq->size--;
}
int SeqListFind(Seqlist* pseq, Datatype x)
//顺序表查找
{
	assert(pseq);

	for (int i = 0; i < pseq->size; ++i)
	{
		if (pseq->arr[i] == x)
		{
			return i;
		}
	}

	return -1;

}

void SeqListInsert(Seqlist* pseq, size_t pos, Seqlist* x)
// 顺序表在pos位置插入x

{
	assert(pseq);
	CheckSeq(pseq);

	size_t end = pseq->size;

	while (end >pos)
	{
		pseq->arr[end] = pseq->arr[end - 1];
		--end;
	}
	
	pseq->arr[pos] = x;
	pseq->size++;
}
void SeqListErase(Seqlist* pseq, size_t pos)
// 顺序表删除pos位置的值
{
	assert(pseq);
	CheckSeq(pseq);

	size_t end = pos ;

	while (end < pseq->size-1)
	{
		pseq->arr[end] = pseq->arr[end + 1];
		++end;
	}

	pseq->size--;
}
void SLModify(Seqlist* psl, size_t pos, Datatype x
{
	assert(psl);
	assert(pos < psl->size);

	psl->arr[pos] = x;
}
//学习了创建堆