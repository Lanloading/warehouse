#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
    int number;
    float C, math, english;
    scanf("%8d;%f,%f,%f", &number, &C, &math, &english);
    printf("The each subject score of No. %d is %.2f, %.2f, %.2f.", number, C, math, english);
    return 0;
}