#define _CRT_SECURE_NO_WARNINGS
#include "Sort.h"
#include"Stackhead.h"

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;

}
void InsertSort(int* a, int n)
{
	for (int i = 0; i < n - 1; ++i)
	{
		int end = i;

		int tmp = a[end + 1];

		while (end >= 0)
		{
			if (a[end] > a[end + 1])
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}

		a[end+1] = tmp;

	}


}


void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int tmp = a[end + gap];

			while (end >= 0)
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}

			a[end + gap] = tmp;
		}
	}

}


void BubbleSort(int* a, int n)
{
	for (int j = 0; j < n; ++j)
	{
		int exchange = 0;
		for (int i = 1; i < n - j; ++i)
		{

			if (a[i-1] > a[i])
			{
				Swap(&a[i - 1], &a[i]);
				exchange = 1;
			}


		}
		if (exchange == 0)
		{
			break;
		}

	}


}


void AdjustDown(int* a, int n, int root)
{
	//找到最小的孩子，默认为左孩子，也就是2i+1

	int minchild = root * 2 + 1;

	while (minchild < n )
	{
		if (a[minchild] < a[minchild + 1] && minchild + 1 < n)
		{
			minchild += 1 ;
		}

		if (a[minchild] > a[root])
		{
			Swap(&a[minchild], &a[root]);
			root = minchild;
			minchild = root * 2 + 1;
		}
		else
		{
			break;
		}

	}

}

void HeapSort(int* a, int n)
{
		for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		//从最后一个叶子的父亲节点开始调整直到根

		AdjustDown(a, n, i);
	}

	for (int i = 1;i<=n;++i)
	{
		Swap(&a[0], &a[n - i]);
		AdjustDown(a, n-i, 0);;
	}


}

//选择排序

void ChooseSort(int* a, int n)
{
	int begin = 0;
	
	int end = n-1;

	while (begin < end)
	{
		int mini = begin;
		int maxi = begin;


		for (int i = begin+1; i <=end; ++i)
		{
			if (a[mini] > a[i])
				mini= i;
			if (a[maxi] < a[i])
				maxi= i;

		}

		Swap(&a[begin], &a[mini]);
		if (maxi == begin)
		{
			maxi = mini;
		}

		Swap(&a[end], &a[maxi]);
		++begin;
		--end;

	}
}

//找中位数
int GetMidIndex(int* a, int left, int right)
{
	int mid = (left + right) / 2;

	if (a[left] < a[mid])
	{
		if (a[right] > a[mid])
			return mid;
		else if (a[right] > a[left])
			return right;
		else
			return left;

	}
	else //left >= mid
	{
		if (a[mid] > a[right])
			return mid;
		else if (a[right] > a[left])
			return left;
		else
			return right;
	}
}






// 快速排序hoare版本

int PartSort1(int* a, int left, int right)
{
	int mid = GetMidIndex(a, left, right);

	Swap(&a[left], &a[mid]);

	int key = left;

	while (left < right)
	{
		//R找比Key小的
		while (left < right && a[right] >= a[key])
		{
			--right;
		}

		//L找比Key大的
		while (left < right && a[left] <= a[key])//为了防止错过的问题发生，记得不要让 left > right
		{
			++left;
		}

		Swap(&a[left], &a[right]);
	}
	int meeti = left;
	Swap(&a[key], &a[left]);
	return meeti;

}


//挖坑法
int PartSort2(int* a, int left, int right)
{
	int mid = GetMidIndex(a, left, right);

	Swap(&a[left], &a[mid]);
	int key = a[left];
	int hole = left;

	while (left<right)
	{
		while (a[right] >= key && left < right)
		{
			--right;
		}

		while (a[left] <= key && left < right)
		{
			++left;
		}

		a[hole ] = a[right];
		hole = right;

		a[hole] = a[left];
		hole = left;

	}
	 a[hole]=key;
	 return hole;
}


//前后指针版本
int PartSort3(int* a, int left, int right)
{

	int mid = GetMidIndex(a, left, right);

	Swap(&a[left], &a[mid]);
	int key = left;
	int prev = left;
	int cur = left + 1;

	while (cur <= right)
	{
		if (a[cur] < a[key] && ++prev != cur)
		{
			Swap(&a[prev], &a[cur]);
		}

		++cur;
	}
	Swap(&a[key], &a[prev]);


	return prev;

}


//非递归版本
void QuickSortNonR(int* a, int left, int right)
{

	Stack st;
	StackInit(&st);
	StackPush(&st, left);
	StackPush(&st, right);

	while (!StackEmpty(&st))
	{
		int right = StackTop(&st);
		StackPop(&st);

		int left = StackTop(&st);
		StackPop(&st);

		/*if (left >= right)
		{
			continue;
		}*/

		int keyi = PartSort3(a, left, right);
		// [left, keyi-1] keyi [keyi+1,right]

		if (keyi + 1 < right)
		{
			StackPush(&st, keyi + 1);
			StackPush(&st, right);
		}

		if (left < keyi - 1)
		{
			StackPush(&st, left);
			StackPush(&st, keyi - 1);
		}
	}

	StackDestroy(&st);

}



void QuickSort(int* a, int begin, int end)
{
	if (begin >= end)
	{
		return;
	}

	int keyi = PartSort3(a, begin, end);
	//[begin, keyi-1] keyi [keyi+1, end]

	QuickSort(a, begin, keyi - 1);
	QuickSort(a, keyi + 1, end);
}


